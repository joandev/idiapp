package joandev.parkmanager.Model;

import android.content.ContentValues;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by joanbarroso on 5/12/14.
 */
public class Park {

    private String name;
    private int sizex;
    private int sizey;
    private String status;

    public void setName(String name) {
        this.name = name;
    }

    public void setSizex(int sizex) {
        this.sizex = sizex;
    }

    public void setSizey(int sizey) {
        this.sizey = sizey;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getStatus() {
        return status;
    }

    public int getSizey() {
        return sizey;
    }

    public int getSizex() {
        return sizex;
    }

    public String getName() {
        return name;
    }

    public Park(int sizeX, int sizeY, String names) {
        sizex = sizeX;
        sizey = sizeY;
        name = names;
        status = "";
        for (int i = 0; i < sizex; i++) {
            for (int j = 0; j < sizey; j++) {
                status += "0";
            }
        }
    }

    public Park(int sizeX, int sizeY, String names, String stats) {
        sizex = sizeX;
        sizey = sizeY;
        name = names;
        status = stats;
    }
}
