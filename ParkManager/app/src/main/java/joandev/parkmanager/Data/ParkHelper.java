package joandev.parkmanager.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import joandev.parkmanager.Model.Park;

/**
 * Created by joanbarroso on 5/12/14.
 */
public class ParkHelper extends SQLiteOpenHelper {


    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Parks";
    public static final String PARK_TABLE = "Park";
    public static final String PARK_TABLE_CREATE = "CREATE TABLE " +
            PARK_TABLE + " (name TEXT PRIMARY KEY UNIQUE,"+ " status TEXT, sizex INTEGER, sizey INTEGER);";

    public ParkHelper(Context context) {
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.v("parkcreate", "WHATSAAAAAP");
        db.execSQL(PARK_TABLE_CREATE);
        Log.v("parkcreate", "WHATSAAAAAP");

    }

    public Cursor getAllParks() {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {"name"};
        Cursor c = db.query(
                PARK_TABLE,  // The table to query
                columns,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );
        return c;
    }

    public Cursor getParkInfo(String parkName) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {"name", "status", "sizex", "sizey"};
        String[] where = {parkName};
        Cursor c = db.query(
                PARK_TABLE,  // The table to query
                columns,                               // The columns to return
                "name=?",                                // The columns for the WHERE clause
                where,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );
        return c;
    }

    public void deletePark(String parkName) {
        SQLiteDatabase db = this.getWritableDatabase();
        String [] name = {parkName};
        db.delete("Park", "name=?", name);
    }

    public void createPark (ContentValues values, String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(
                tableName,
                null,
                values);
    }

    public void updatePark(ContentValues values, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        String [] st = {name};
        db.update("Park", values, "name=?", st);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
