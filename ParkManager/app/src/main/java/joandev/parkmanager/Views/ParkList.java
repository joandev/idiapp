package joandev.parkmanager.Views;

import android.app.Activity;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import joandev.parkmanager.Data.ParkHelper;
import joandev.parkmanager.R;

public class ParkList extends ListActivity {

    ArrayList<String> listItems=new ArrayList<String>();
    ArrayAdapter<String> adapter;
    ParkHelper parkHelper;
    String parkSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_park_list);


        super.onCreate(savedInstanceState);

        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);
        setListAdapter(adapter);

        parkHelper = new ParkHelper(getApplicationContext());

        Cursor c = parkHelper.getAllParks();
        if (c.moveToFirst()) {
            do {
                adapter.add(c.getString(c.getColumnIndex("name")));
            } while (c.moveToNext());
        }
        ListView lv = getListView();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v,int position, long arg3)
            {
                parkSelected = ((TextView) v).getText().toString();
                goToPark();
            }
        });

    }

    private void goToPark() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("fromDB", true);
        Cursor c = parkHelper.getParkInfo(parkSelected);
        if (c.moveToFirst()) {
            bundle.putInt("sizex", c.getInt(c.getColumnIndex("sizex")));
            bundle.putInt("sizey", c.getInt(c.getColumnIndex("sizey")));
            bundle.putString("status", c.getString(c.getColumnIndex("status")));
        }
        bundle.putString("name", parkSelected);
        Intent intent = new Intent(getApplicationContext(), ParkActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MyActivity.class);
        startActivity(intent);
        finish();
    }


}
