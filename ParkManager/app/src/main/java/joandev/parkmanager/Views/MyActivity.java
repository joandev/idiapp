package joandev.parkmanager.Views;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import joandev.parkmanager.R;


public class MyActivity extends Activity {



    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);


        Button myParks = (Button) findViewById(R.id.myParks);
        Button createPark = (Button) findViewById(R.id.createPark);
        Button about = (Button) findViewById(R.id.about);

        GradientDrawable gd = (GradientDrawable)getResources().getDrawable(R.drawable.menu_button);
        myParks.setBackground(gd);

        createPark.setBackground(gd);
        about.setBackground(gd);
    }

    public void loadParksSelection(View view) {
        Intent intent = new Intent(this, ParkList.class);
        startActivity(intent);
    }

    public void loadParkCreation(View view) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("modify", false);
        Intent intent = new Intent(this, ParkCreator.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void goToAbout (View v) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
}
