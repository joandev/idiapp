package joandev.parkmanager.Views;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import joandev.parkmanager.Data.ParkHelper;
import joandev.parkmanager.Model.Park;
import joandev.parkmanager.R;


public  class ParkActivity extends Activity {

    GridLayout gridLayout;
    Park myPark;
    int parkSizeX;
    int parkSizeY;
    String parkName;
    String status;
    boolean fromDB = false;
    boolean modified = false;
    ParkHelper ph;

    Button saveButton;
    Button deleteButton;
    Button parkList;
    Button editPark;
    TextView parkname;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_park);

        saveButton = (Button) findViewById(R.id.savePark);
        deleteButton = (Button) findViewById(R.id.deletePark);
        parkList = (Button) findViewById(R.id.parkList);
        editPark = (Button) findViewById(R.id.editPark);
        parkname = (TextView) findViewById(R.id.parkName);

        ph = new ParkHelper(getApplicationContext());

        Bundle bundle = getIntent().getExtras();

        GradientDrawable normal = (GradientDrawable)getResources().getDrawable(R.drawable.menu_button);
        parkList.setBackground(normal);
        editPark.setBackground(normal);

        GradientDrawable saveBackground = (GradientDrawable)getResources().getDrawable(R.drawable.save_button);
        saveButton.setBackground(saveBackground);

        GradientDrawable deleteBackground = (GradientDrawable)getResources().getDrawable(R.drawable.delete_button);
        deleteButton.setBackground(deleteBackground);

        parkSizeX = bundle.getInt("sizex");
        parkSizeY = bundle.getInt("sizey");
        parkName = bundle.getString("name");
        parkname.setText(parkName);
        if (bundle.getBoolean("fromDB")){
            fromDB = true;
            myPark = new Park(parkSizeX,parkSizeY,parkName,bundle.getString("status"));
        }
        else{
            myPark = new Park(parkSizeX,parkSizeY,parkName);
        }
        status = myPark.getStatus();



//      gridLayout = new GridLayout(ParkActivity.this);
        gridLayout = (GridLayout)findViewById(R.id.zonesGridLayout);
        gridLayout.setColumnCount(parkSizeY);
        gridLayout.setRowCount(parkSizeX);
        gridLayout.setOrientation(GridLayout.HORIZONTAL);
        Log.v("startPark", "hahahha");


        int cont = 0;
        for (int i = 0; i < parkSizeX; i++) {
            for (int j = 0; j < parkSizeY; j++) {
                Button btn = new Button(this);
                btn.setId(cont);
                btn.setTextColor(0xFFF5F5F5);
                btn.setText("" + i + ":" + j);
                btn.setTextSize(17);
                btn.layout(1,1,1,1);
                btn.setOnTouchListener(onTouchListener);
                if (status.charAt(cont) == '0'){
                    GradientDrawable sd = (GradientDrawable)getResources().getDrawable(R.drawable.custom_button);
                    sd.setColor(0xFFDE332A);
                    btn.setBackground(sd);
                }
                else {
                    GradientDrawable sd = (GradientDrawable)getResources().getDrawable(R.drawable.custom_button);
                    sd.setColor(0xFF4FD853);
                    btn.setBackground(sd);
                }
                btn.setBackground(getResources().getDrawable(R.drawable.custom_button));
                GridLayout.Spec row = GridLayout.spec(i, 1);
                GridLayout.Spec colspan = GridLayout.spec(j, 1);
                GridLayout.LayoutParams gridLayoutParam = new GridLayout.LayoutParams(row, colspan);
                gridLayoutParam.setMargins(1, 1, 1, 1);
                gridLayout.addView(btn,gridLayoutParam);
                ++cont;
            }
        }
    }





    View.OnTouchListener onTouchListener =  new View.OnTouchListener() {

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public boolean onTouch(View v, MotionEvent event) {


            if (event.getAction() == event.ACTION_UP) {
                GradientDrawable sd = (GradientDrawable) v.getBackground();
                Log.v("entroButton", status);
                if (status.charAt(v.getId()) == '0') {
                    sd.setColor(0xFF4FD853);
                    char[] auxiliar = status.toCharArray();
                    auxiliar[v.getId()] = '1';
                    status = String.valueOf(auxiliar);
                } else {
                    sd.setColor(0xFFDE332A);
                    char[] auxiliar = status.toCharArray();
                    auxiliar[v.getId()] = '0';
                    status = String.valueOf(auxiliar);
                }
                Log.v("entroButton", status);
                v.setBackground(sd);
            }
            modified = true;
            return true;

        }

    };

    public void storeCurrentPark (View v) {
        ContentValues valuesToStore = new ContentValues();
        valuesToStore.put("name", parkName);
        valuesToStore.put("status", status);
        valuesToStore.put("sizex", parkSizeX);
        valuesToStore.put("sizey", parkSizeY);
        ph.updatePark(valuesToStore, parkName);
        modified = false;
        new AlertDialog.Builder(this)
                .setTitle("Park saved")
                .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void deletePark(View v) {

        GradientDrawable deleteBackgroundPressed = (GradientDrawable)getResources().getDrawable(R.drawable.delete_button_pressed);
        deleteButton.setBackground(deleteBackgroundPressed);

        new AlertDialog.Builder(this)
                .setTitle("Delete park")
                .setMessage("Are you sure you want to delete this park?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ph.deletePark(parkName);
                        goToParkList();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
        GradientDrawable deleteBackground = (GradientDrawable)getResources().getDrawable(R.drawable.delete_button);
        deleteButton.setBackground(deleteBackground);
    }

    private void goToParkList() {
        Intent intent = new Intent(this, ParkList.class);
        startActivity(intent);
        finish();
    }

    public void backToList(View v) {
        goToParkList();
    }

    public void modifyPark(View v) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("modify", true);
        bundle.putString("name", parkName);
        bundle.putInt("sizex", parkSizeX);
        bundle.putInt("sizey", parkSizeY);
        Intent intent = new Intent(this, ParkCreator.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (modified){
            new AlertDialog.Builder(this)
                    .setTitle("Leaving the park")
                    .setMessage("You will loose not saved changes")
                    .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .show();
        }
        else {
            finish();
        }

    }
}
