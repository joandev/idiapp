package joandev.parkmanager.Views;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import joandev.parkmanager.Data.ParkHelper;
import joandev.parkmanager.Model.Park;
import joandev.parkmanager.R;

import static joandev.parkmanager.R.id.parkNameTV;

public class ParkCreator extends Activity {

    EditText parkName;
    SeekBar parkSizeX;
    SeekBar parkSizeY;
    TextView parkWidthNumber;
    TextView parkHeightNumber;
    TextView activityTitle;
    int sizeX = 1;
    int sizeY = 1;
    String name;
    ParkHelper ph;

    Button createButton;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_park_creator);

        activityTitle = (TextView) findViewById(R.id.textView2);
        parkName = (EditText) findViewById(R.id.parkNameTV);
        parkSizeY = (SeekBar) findViewById(R.id.seekBar);
        parkSizeX = (SeekBar) findViewById(R.id.seekBar2);
        parkHeightNumber = (TextView)findViewById(R.id.parkHeightNumber);
        parkWidthNumber = (TextView)findViewById(R.id.parkWidthNumber);
        createButton = (Button) findViewById(R.id.createParkButton);

        GradientDrawable gd = (GradientDrawable)getResources().getDrawable(R.drawable.menu_button);
        createButton.setBackground(gd);

        Bundle bundle = getIntent().getExtras();
        if (bundle.getBoolean("modify")) {
            parkName.setText(bundle.getString("name"));
            parkName.setClickable(false);
            parkName.setFocusable(false);
            createButton.setText("Save changes");
            int sx = bundle.getInt("sizex");
            parkSizeX.setProgress(sx-1);
            parkHeightNumber.setText("" + sx);
            int sy = bundle.getInt("sizey");
            parkSizeY.setProgress(sy-1);
            parkWidthNumber.setText("" + sy);
            activityTitle.setText("Edit your park characteristics");
        }

        ph = new ParkHelper(getApplicationContext());


        parkSizeY.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                parkWidthNumber.setText(String.valueOf(progress+1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
        });

        parkSizeX.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                parkHeightNumber.setText(String.valueOf(progress+1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
        });

    }


    public void createPark (View view) {
        name = parkName.getText().toString();
        boolean firstExcepcionThrown = false;
        name = name.trim();
        if (name.length() == 0) {
            new AlertDialog.Builder(this)
                    .setTitle("Invalid name")
                    .setMessage("Please insert the park's name")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();        }
        else {
            sizeX = parkSizeX.getProgress()+1;
            sizeY = parkSizeY.getProgress()+1;
            Bundle bundle = new Bundle();
            bundle.putBoolean("fromDB", false);
            bundle.putInt("sizex", sizeX);
            bundle.putInt("sizey", sizeY);
            bundle.putString("name",name);
            Intent intent = new Intent(this, ParkActivity.class);
            intent.putExtras(bundle);
            insertParkInDB();
            startActivity(intent);
            finish();
        }
    }

    private void insertParkInDB() {
        ContentValues valuesToStore = new ContentValues();
        String status = new String("");
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                status += "0";
            }
        }
        valuesToStore.put("name", name);
        valuesToStore.put("status", status);
        valuesToStore.put("sizex", sizeX);
        valuesToStore.put("sizey", sizeY);
        ph.createPark(valuesToStore,"Park");
    }
}
